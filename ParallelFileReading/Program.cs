﻿/*Параллельное считывание файлов
Цель:

Студент сделает запуск тасок в параллель, тем самым обретя базовые навыки работы с тасками, 
что необходимо в повседневной работе C#-программиста

Описание/Пошаговая инструкция выполнения домашнего задания:

1    Прочитать 3 файла параллельно и вычислить количество пробелов в них (через Task).
2    Написать функцию, принимающую в качестве аргумента путь к папке. Из этой папки параллельно 
    прочитать все файлы и вычислить количество пробелов в них.
3    Замерьте время выполнения кода (класс Stopwatch).*/

using System.Diagnostics;

namespace ParallelFileReading;

internal class Program
{
    static async Task Main()
    {
        const string directoryPath = ".";
        if (!Directory.Exists(directoryPath))
        {
            throw new DirectoryNotFoundException();
        }
        Console.WriteLine("Counting white spaces in all files");
        Console.WriteLine($"\tin directory: {Path.GetFullPath(directoryPath)}\n");
        Dictionary<string, int> dict = await CalculateSpacesInDirectoryAsync(directoryPath);
        foreach (var kvp in dict)
        {
            Console.WriteLine($"{kvp.Key} has {kvp.Value} spaces");
        }
        // Это не-параллельный вариант для сравнения
        Console.WriteLine("\nLet's measure non parallel variant too:");
        dict = await CalculateSpacesInDirectoryNonParallelAsync(directoryPath);
        foreach (var kvp in dict)
        {
            Console.WriteLine($"{kvp.Key} has {kvp.Value} spaces");
        }
    }

    static async Task<Dictionary<string,int>> CalculateSpacesInDirectoryAsync(string directoryPath)
    {
        Stopwatch stopwatch = Stopwatch.StartNew();
        string[] filePaths = Directory.GetFiles(directoryPath);
        Task<int>[] tasks = new Task<int>[filePaths.Length];
        for (int i = 0; i < filePaths.Length; i++)
        {
            tasks[i] = CalculateSpacesInFileAsync(filePaths[i]);
        }
        await Task.WhenAll(tasks);

        Dictionary<string, int> resultDictionary = new();
        for (int i = 0;i < filePaths.Length; i++)
        {
            resultDictionary[filePaths[i]] = tasks[i].Result;
        }
        stopwatch.Stop();
        Console.WriteLine($"The operation took {stopwatch}\n");
        return resultDictionary;
    }

    static async Task<int> CalculateSpacesInFileAsync(string filePath)
    {
        string text = await File.ReadAllTextAsync(filePath);
        return text.Count(char.IsWhiteSpace);
    }

    // Это не-параллельный вариант для сравнения
    static async Task<Dictionary<string, int>> CalculateSpacesInDirectoryNonParallelAsync(string directoryPath)
    {
        Stopwatch stopwatch = Stopwatch.StartNew();
        string[] filePaths = Directory.GetFiles(directoryPath);
        Dictionary<string, int> resultDictionary = new();
        foreach (var filePath in filePaths)
        {
            resultDictionary[filePath] = await CalculateSpacesInFileAsync(filePath);
        }
        stopwatch.Stop();
        Console.WriteLine($"The operation took {stopwatch}\n");
        return resultDictionary;
    }
}
